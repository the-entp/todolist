package com.example.todolistreader;

import java.util.ArrayList;
import java.util.Locale;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;
import android.speech.tts.TextToSpeech;
import android.util.Log;

public class Text2Speech extends Service implements TextToSpeech.OnInitListener {
	private TextToSpeech tts;
	
	public void onCreate() {
		Log.d("SpeakerService","Service created successfully!");
		tts = new TextToSpeech(getApplicationContext(),this);
		tts.setLanguage(Locale.ENGLISH);
		
	}
	
	@Override
	public void onStart(Intent intent,int startid)
	  {
	Log.d("SpeakerService","Service started successfully!");
	 Log.d("SpeakerService","Service started successfully!");
	 tts = new TextToSpeech(getApplicationContext(),this);
	 tts.setLanguage(Locale.ENGLISH);
	}
	@Override
	public void onInit(int arg0) {
		if (arg0 == TextToSpeech.SUCCESS) {
			 
            int result = tts.setLanguage(Locale.US);
 
            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.v("text", "Language is not supported");
            } else {
                speakList();
            }
        } else {
            Log.e("TTS", "Initilization Failed!");
        }
 
		
	}
	
	public void speakList() {
		Log.v("hi", "speaklist");
		ArrayList<String> toDoList = MainActivity.getTdList();
		for (int i =0; i < toDoList.size(); i++) {
			System.out.println("This is i" + i + " " + toDoList.get(i));
			tts.speak(toDoList.get(i), TextToSpeech.QUEUE_FLUSH, null);
			while(tts.isSpeaking()) {		
			}
			SystemClock.sleep(1000);
			
			
		}
		System.out.println(tts);
		//tts.speak("hello peeps", TextToSpeech.QUEUE_FLUSH, null); 
	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void onDestroy() {
		if (tts != null) {
			tts.stop();
			tts.shutdown();
		}
		super.onDestroy();
		System.out.println("Destroyed");
	}

}
