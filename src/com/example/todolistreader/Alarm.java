package com.example.todolistreader;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import android.speech.tts.TextToSpeech;



import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

@SuppressLint("Wakelock")
public class Alarm extends BroadcastReceiver {
	

	@Override
	public void onReceive(Context arg0, Intent arg1) {
		CharSequence text = "Hello toast!";
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(arg0, text, duration);
		toast.show();;
		PowerManager pm = (PowerManager) arg0.getSystemService(Context.POWER_SERVICE);
        @SuppressWarnings("deprecation")
		PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, getClass().getName());
        wl.acquire(1000*60*5);
       Intent installIntent = new Intent(arg0, Text2Speech.class);      
         arg0.startService(installIntent);
//        final TextToSpeech talker = new TextToSpeech(arg0, new TextToSpeech.OnInitListener() {
//
//            @Override
//            public void onInit(int arg0) {
//           if(arg0 == TextToSpeech.SUCCESS) 
//               {
//        	 
//            talker.setLanguage(Locale.US);
//            //talker.speak("hi", TextToSpeech.QUEUE_FLUSH, null); 
//              
//                }
//            }
//        });
     
        
        
       
	     
	}
	
	public void testTriggerTime(long triggerTime) {
		Calendar rightNow = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS",Locale.US);
        GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("US/Central"));
        calendar.setTimeInMillis(triggerTime);
        System.out.println("Right now: " + sdf.format(rightNow.getTime()));
        System.out.println("GregorianCalendar -"+sdf.format(calendar.getTime()));
	}

    public void setAlarm(Context context, long triggerTime)
    {
    	System.out.println("my alarm is here" + triggerTime);
    	testTriggerTime(triggerTime);
        AlarmManager am=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, Alarm.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
        am.setRepeating(AlarmManager.RTC_WAKEUP, triggerTime, 1000 * 60 *60*24, pi); // Millisec * Second * Minute
    }

    public void cancelAlarm(Context context)
    {
        Intent intent = new Intent(context, Alarm.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }



}
