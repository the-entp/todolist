package com.example.todolistreader;

import android.provider.BaseColumns;

public final class ToDoContract {
    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public ToDoContract() {}

    /* Inner class that defines the table contents */
    public static abstract class ToDoEntry implements BaseColumns {
        public static final String TABLE_NAME = "ToDoList";
        public static final String TO_DO_ITEM = "ToDoItem";
    }
}
