package com.example.todolistreader;

import java.util.ArrayList;

import com.example.todolistreader.ToDoContract.ToDoEntry;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

@SuppressLint("NewApi")
public class MainActivity extends FragmentActivity {
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ListView mDrawerList;
    private ToDoDbHelper tdDbHelper = new ToDoDbHelper(this);
    static ArrayList<String> todoList = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		getData();
		
		setUpNavigationDrawer();

	}
	
	public static ArrayList<String> getTdList() {
		return todoList;
	}
	
	public void getData() {
		SQLiteDatabase db = tdDbHelper.getReadableDatabase();

		// Define a projection that specifies which columns from the database
		// you will actually use after this query.
		String[] projection = {
		    ToDoEntry._ID,
		    ToDoEntry.TO_DO_ITEM
		    };

		// How you want the results sorted in the resulting Cursor
		String sortOrder =
		    ToDoEntry._ID + " ASC";

		Cursor c = db.query(
		    ToDoEntry.TABLE_NAME,  // The table to query
		    projection,                               // The columns to return
		    null,                                // The columns for the WHERE clause
		    null,                            // The values for the WHERE clause
		    null,                                     // don't group the rows
		    null,                                     // don't filter by row groups
		    sortOrder                                 // The sort order
		    );
		 int todoIndex=c.getColumnIndex(ToDoEntry.TO_DO_ITEM);
		 for(c.moveToFirst(); !c.isAfterLast();c.moveToNext()){
		        todoList.add(c.getString(todoIndex));
		    }
	}
	
	public void stopSpeech(View v) {
		System.out.println("in stop speech");
		Intent installIntent = new Intent(this, Text2Speech.class);      
        stopService(installIntent);
	}
	
	public void setUpNavigationDrawer() {
		 mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		  // Set the adapter for the list view
		 mDrawerList = (ListView) findViewById(R.id.left_drawer);
		 mDrawerList.setAdapter(new ArrayAdapter<String>(this,
	                R.layout.drawer_list_item,todoList));
	     mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
	     
	     getActionBar().setDisplayHomeAsUpEnabled(true);
	     getActionBar().setHomeButtonEnabled(true);
	       
	        // Set the list's click listener
	     mDrawerToggle = new ActionBarDrawerToggle(
	                this,                  /* host Activity */
	                mDrawerLayout,         /* DrawerLayout object */
	                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
	                R.string.drawer_open,  /* "open drawer" description */
	                R.string.drawer_close  /* "close drawer" description */
	                ) {

	            /** Called when a drawer has settled in a completely closed state. */
	            public void onDrawerClosed(View view) {
	            	System.out.println("Drawer closed ");
	                getActionBar().setTitle(getTitle());
	                invalidateOptionsMenu();
	            }

	            /** Called when a drawer has settled in a completely open state. */
	            public void onDrawerOpened(View drawerView) {
	            	System.out.println("Drawer opened ");
	                getActionBar().setTitle(getTitle());
	                invalidateOptionsMenu();
	            }
	        };
	        // Set the drawer toggle as the DrawerListener
	        mDrawerLayout.setDrawerListener(mDrawerToggle);
		
	}
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}
	
	// neccessary to get navigation drawer to work
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
      if (mDrawerToggle.onOptionsItemSelected(item)) {
          return true;
      }
      return false;
	}

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	
	public void saveInDatabase(View v) {
		System.out.println("i am here");
		EditText text = (EditText) findViewById(R.id.item);
		String info = text.getText().toString();
		if (info.equals("")) { // prevents null values from being added
			return;
		}
		System.out.println("Hi i am here " + info);
		SQLiteDatabase db = tdDbHelper.getWritableDatabase();
		// Create a new map of values, where column names are the keys
		ContentValues values = new ContentValues();
		values.put(ToDoEntry.TO_DO_ITEM, info);

		// Insert the new row, returning the primary key value of the new row
		long newRowId;
		newRowId = db.insert(
		         ToDoEntry.TABLE_NAME,
		         null, //cannot insert null values
		         values);
		todoList.add(info);
		text.setText("");
	}
	public void showTimePickerDialog(View v) {
	    DialogFragment newFragment = new TimePickerFragment(this);
	    newFragment.show(getSupportFragmentManager(), "timePicker");
	}
	
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
	    @Override
	    public void onItemClick(AdapterView parent, View view, int position, long id) {
//	        selectItem(position);
	    }
	}

}
